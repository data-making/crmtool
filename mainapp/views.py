from django.shortcuts import render

# Create your views here.

def home(request):

    user = request.user
    print(user)
    print(type(user))
    print(dir(user))
    print(user.get_username().title())

    print(len(user.username))
    username = user.username
    if len(username) > 0:
        username = username.title()
    print(username)

    data = {"username": username}
    return render(request, "mainapp/home.html", context = data)
