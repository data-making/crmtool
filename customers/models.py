from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Customer(models.Model):
    customer_id = models.AutoField(primary_key=True)
    customer_name = models.CharField(max_length=100)
    customer_email = models.EmailField(max_length=200)
    customer_phone = models.IntegerField(max_length=13)
    customer_city = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'customer_tbl'
