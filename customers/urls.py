from django.urls import path

from . import views

urlpatterns = [
    path("add-customer/", views.add_customer, name='add_customer'),
    #/customers/edit-customer/1
    path("edit-customer/<int:id>", views.edit_customer, name='edit_customer'),
    path("view-customer/<int:id>", views.view_customer, name='view_customer'),    
    path("delete-customer/<int:id>", views.delete_customer, name='delete_customer'), 
]