from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def signin(request):
    # 1. AuthenticationForm
    # 2. authenticate
    # 3. login

    print(request.method)

    if request.method == "GET":
        form = AuthenticationForm()

        data = {"login_form": form}
    else:
        print("Else loop")
        print(request.POST)
        form = AuthenticationForm(data = request.POST)

        data = {"login_form": form}

        if form.is_valid():
            print("Form is valid")
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # authenticate - user, password - identity check
            user = authenticate(username=username, password=password)
            #user = User.objects.filter(username = username)
            
             # login - user, password - store the user in session
            #login(username, password) - wrong
            #login(request, username) - wrong
            #login(request, user[0])
            if user is not None:
                # login(request_object, user_object)
                login(request, user)
                return redirect("home")
        else:
            print("Form is invalid")

    return render(request, "accounts/signin.html", context=data)


def signup(request):
    print(request.method)

    if request.method == "GET":
        form = UserCreationForm()
        data = {"form": form}

        #data = { "name": "Rakesh" }

        return render(request, "accounts/signup.html", context=data)
    else: # request.method == "POST"
        print("Printing POST request values: ")
        print(request.POST)

        form = UserCreationForm(data = request.POST)
        data = {"form": form}

        #username = request.POST["username"]
        #password1 = request.POST["password1"]
        #password2 = request.POST["password2"]

        # check if user exists
        # select * from auth_user
        #user = User.objects.filter(username=username)
        #print(user)

        # check password1 = password2
        #if password1 == password2:
        #    pass
        #else:
        #    print("Password does not match")

        if form.is_valid():
            username = form.cleaned_data["username"]
            print("Printing username: ")
            print(username)
            #password1 = form.cleaned_data["password1"]
            #password2 = form.cleaned_data["password2"]

            user = form.save()

            if user is not None:
                #return redirect("signin") 
                return render(request, "accounts/signin.html")
            else:
                return render(request, "accounts/signup.html", context=data)
        else:
            return render(request, "accounts/signup.html", context=data)
        

def signout(request):
    logout(request)

    #return render(request, "mainapp/home.html")
    #return redirect("home")
    return render(request, "accounts/signout-success.html")
            